﻿// 19.5 1 попытка.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <string>

using namespace std;

class Animal
{
public:
	virtual void Voice()
	{
		cout << "voice" << endl;
	}
};

class Dog : Animal
{
public:
	void Voice() override
	{
		cout << " The Dog says - Woof " << endl;
	}
};

class Cat : Animal
{
public:
	void Voice() override
	{
		cout << " The Cat says - Meow " << endl;
	}
};

class Cow: Animal
{
public:
	void Voice() override
	{
		cout << " The Cow says - Muuu " << endl;
	}
};


int main()
{
	Dog bark;
	bark.Voice();

	Cat soften;
	soften.Voice();

	Cow lowing;
	lowing.Voice();

	cout << "======================================" << endl;

	int const SIZE = 3;
	Animal *arr[SIZE];
	{
		arr[0] = &bark;
		arr[1] = Cat;
		arr[2] = *Cow;
		
	}

}
